import { SET_TICKERS } from './types'

export const setTickers = (tickers) => dispatch => {
  dispatch({
    type: SET_TICKERS,
    payload: tickers
  })
}