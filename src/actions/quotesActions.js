import { SET_QUOTE } from './types'
import axios from 'axios'

export const getQuotes = (tickers) => dispatch => {
  getQuote(tickers.long).then((res) => {
    let data = {
      long: res.data
    }
    getQuote(tickers.short).then((res) => {
      data.short = res.data
      dispatch({
        type: SET_QUOTE,
        payload: data
      })
    })

  })
  
}

const getQuote = (ticker) => {
  return axios.get(`https://api.iextrading.com/1.0/stock/${ticker}/quote`)
}

