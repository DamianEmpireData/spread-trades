import { SET_CHARTS, SET_TIMEFRAME, SET_CHART_CLICK } from './types'
import axios from 'axios'

export const getCharts = (tickers, tf) => dispatch => {
  return new Promise((resolve, reject) => {
    getChart(tickers.long, tf).then((res) => {
      let data = {
        long: res.data
      }
      getChart(tickers.short, tf).then((res) => {
        data.short = res.data
        dispatch({
          type: SET_CHARTS,
          payload: data
        })
        resolve()
      })

    })
  })
  
}

export const setTimeFrame = (tf) => dispatch => {
  dispatch({
    type: SET_TIMEFRAME,
    payload: tf
  })
}

export const setChartClick = (data) => dispatch => {
  dispatch({
    type: SET_CHART_CLICK,
    payload: data
  })
}

const getChart = (ticker, timeframe = 'ytd') => {
  return axios.get(`https://api.iextrading.com/1.0/stock/${ticker}/chart/${timeframe}`)
}

