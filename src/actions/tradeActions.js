import { SET_ENTRY_DATA, SET_OUT_DATA, RESET_TRADE_DATA } from './types'

export const setEntryData = (data) => dispatch => {
  dispatch({
    type: SET_ENTRY_DATA,
    payload: data
  })
}

export const setOutData = (data) => dispatch => {
  dispatch({
    type: SET_OUT_DATA,
    payload: data
  })
}

export const resetTradeData = () => dispatch => {
  dispatch({
    type: RESET_TRADE_DATA
  })
}