import React, { Component } from 'react';
import { Provider } from 'react-redux'
import store from './store'
import Tickers from './components/Tickers'
import Quotes from './components/Quotes'
import Chart from './components/Chart'

import './App.css';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Tickers/>
          <Quotes/>
          <Chart/>
        </div>
      </Provider>
    );
  }
}

export default App;
