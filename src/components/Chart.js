import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import sma from 'sma'
import {
  ResponsiveContainer,
  CartesianGrid,
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip
} from 'recharts';
import { getCharts, setTimeFrame  } from '../actions/chartsActions'
import { setEntryData, setOutData } from '../actions/tradeActions'
import MACheckbox from './MACheckBox'

class Chart extends Component {
  constructor(props) {
    super(props)

    this.state = {
      ratios: [],
      calculated: false,
      sma20: false,
      sma60: false,
      sma120: false,
    }

    this.calculateRatios = this.calculateRatios.bind(this)
    this.renderCharts = this.renderCharts.bind(this)
    this.clickedOnMap = this.clickedOnMap.bind(this)
    this.renderSMA = this.renderSMA.bind(this)
    this.addSMA = this.addSMA.bind(this)
    this.changeTimeFrame = this.changeTimeFrame.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!_.isEqual(this.props.tickers, nextProps.tickers)) {
      this.props.getCharts(nextProps.tickers, nextProps.tf).then(() => {
        this.calculateRatios()
      })
    }

    return true
  }

  calculateRatios() {
    if(_.isEmpty(this.props.long))
    {
      console.log('empty')
      return;
    }

    let short = this.props.short

    let ratios = []
    let r_sma = []

    this.props.long.forEach((item, index) => {
      let s = short[index]
      let r =Number((item.close / s.close).toFixed(2))

      let data = {
        date: item.date,
        ratio: r
      }

      r_sma.push(r)
      ratios.push(data)
    })

    ratios = this.calculateSMA(ratios, r_sma, 20)
    ratios = this.calculateSMA(ratios, r_sma, 60)
    ratios = this.calculateSMA(ratios, r_sma, 120)
    

    this.setState({
      ratios: ratios,
      calculated: true
    })
  }

  calculateSMA(array, ratios, s = 20)  {
      let ma = sma(ratios, s)
      array.forEach((item, index) => {
          if(index >= s)
          {
            item['sma' + s] = Number(ma[(index - s)])
          }
      })

      return array
  }

  clickedOnMap(data, index) {
    if(_.isEmpty(data) || _.isEmpty(this.props.click))
      return

    console.log(data)

    
    let ratio = data.activePayload[0].payload.ratio
    let date = data.activeLabel

    let l = this.getPrice(this.props.long, date)
    let s = this.getPrice(this.props.short, date)

    let object = {
      long_price: Number((l[0].close).toFixed(2)),
      short_price: Number((s[0].close).toFixed(2)),
      ratio: ratio,
      date: data.activeLabel
    }

    if(this.props.click === 'entry')
    {
      this.props.setEntryData(object)
    }else {
      this.props.setOutData(object)
    }
  }

  getPrice(item, date) {
    return item.filter((item) => {
      return item.date === date
    })
  }

  renderSMA (datakey, name, color = '#fff') {
    if(this.state[datakey]) {
      return (
        <Line 
          type="monotone"
          name={name}
          dataKey={datakey}
          stroke={color}
          strokeWidth={1}
          dot={false}
        />
      )
    }
  }

  addSMA(event) {
    let name = event.target.name

    this.setState({
      [name]: !this.state[name]
    })
  }

  changeTimeFrame(event) {
    this.props.setTimeFrame(event.target.value)

    this.props.getCharts(this.props.tickers, event.target.value).then(() => {
      this.calculateRatios()
    })
  }

  renderCharts () {
      return (
        <ResponsiveContainer height={500} width="100%">
          <LineChart data={this.state.ratios} onClick={this.clickedOnMap}>
            <CartesianGrid strokeDasharray="3 3" stroke="#ccc" strokeOpacity={.2} />
            <Line 
              type = "monotone"
              dataKey = "ratio"
              name="Ratio"
              stroke= "#a0ffff"
              strokeWidth={3}
              dot = {false}
            />
            {this.renderSMA('sma20', 'MA 20', '#fcb8a1')}
            {this.renderSMA('sma60', 'MA 60', '#d4fca4')}
            {this.renderSMA('sma120', 'MA 120', '#f4e242')}
            <XAxis 
              dataKey = "date"
              stroke= "#a0ffff"
              tick={{ fontSize: 10}}
            >
            </XAxis>
            
            <YAxis type="number" domain={['auto', 'auto']} />
            <Tooltip 
              labelStyle={{color: '#000'}}
              itemStyle={{color: '#000'}} />
          </LineChart>
        </ResponsiveContainer>
      )
  }

  render() {
    return (
      <div style={{color: '#fff'}}>
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-4">
            <label style={{display: 'block'}}>Time Frame</label>
            <select defaultValue={this.props.tf} onChange={this.changeTimeFrame}>
              <option value="1m">1m</option>
              <option value="3m">3m</option>
              <option value="6m">6m</option>
              <option value="ytd">YTD</option>
              <option value="1y">1Y</option>
              <option value="2y">2Y</option>
              <option value="5y">5Y</option>
            </select>
          </div>
          <div className="col-lg-4">
            <label style={{display: 'block'}}>Moving Avarages</label>
            <MACheckbox name="sma20" sma="20" value={this.state.sma20} onChange={this.addSMA} />
            <MACheckbox name="sma60" sma="60" value={this.state.sma60} onChange={this.addSMA} />
            <MACheckbox name="sma120" sma="120" value={this.state.sma120} onChange={this.addSMA} />
          </div>
        </div>
      </div>
        {this.renderCharts()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tickers: state.tickers,
  long: state.charts.long,
  short: state.charts.short,
  tf: state.charts.tf,
  click: state.charts.click
})

export default connect(mapStateToProps, {getCharts, setTimeFrame, setEntryData, setOutData })(Chart)