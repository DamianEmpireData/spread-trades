import React, { Component } from 'react'

class MACheckbox extends Component {
  constructor(props) {
    super(props)

    this.changeHandler = this.changeHandler.bind(this)
  }

  changeHandler (event) {
    this.props.onChange(event)
  }

  render() {
    return (
      <div className="form-check form-check-inline">
        <input className="form-check-input" type="checkbox" name={this.props.name} value={this.props.value} onChange={this.changeHandler} />
        <label className="form-check-label">{this.props.sma}</label>
      </div>
    )
  }

}

export default MACheckbox