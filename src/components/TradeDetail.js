import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

class TradeDetail extends Component {
  constructor(props) {
    super(props)
    this.calculateSharesAmount = this.calculateSharesAmount.bind(this)
    this.setSize = this.setSize.bind(this)
    this.calculatePandL = this.calculatePandL.bind(this)
    this.calculateYield = this.calculateYield.bind(this)
    this.calculatePositionSize = this.calculatePositionSize.bind(this)

    this.state = {
      size: 1000,
      long: 0,
      short: 0
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!_.isEqual(this.state, prevState)) {
      this.calculateSharesAmount(this.props.trade)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.trade, nextProps.trade)) {
      this.calculateSharesAmount(nextProps.trade)
    }
    return true
  }

  setSize(event) {
    this.setState({
      size: event.target.value
    })
  }

  calculateSharesAmount(data) {
    let long = (this.state.size / 2) / data.entry.long_price
    let short = (this.state.size / 2) / data.entry.short_price

    this.setState({
      long: Math.round(long),
      short: Math.round(short)
    })
  }

  calculatePositionSize() {
    if (this.props.trade.entry.long_price === 0 || this.state.long === 0) {
      return 0
    }

    let long = this.state.long * this.props.trade.entry.long_price
    let short = this.state.short * this.props.trade.entry.short_price

    let total = Number((long + short).toFixed(2))

    return `$ ${total}`
  }

  calculatePandL() {
    if (this.props.trade.out.long_price === 0) {
      return 0
    }

    let long_cost = this.state.long * this.props.trade.entry.long_price
    let short_cost = this.state.short * this.props.trade.entry.short_price
    let long_result = this.state.long * this.props.trade.out.long_price
    let short_result = this.state.short * this.props.trade.out.short_price

    let long_total = long_result - long_cost
    let short_total = short_cost - short_result

    let total = Number((long_total + short_total).toFixed(2))

    if (_.isNaN(total))
      return 0

    return `$ ${total}`
  }

  calculateYield() {
    if (this.props.trade.out.long_price === 0) {
      return 0
    }

    let long_cost = this.state.long * this.props.trade.entry.long_price
    let short_cost = this.state.short * this.props.trade.entry.short_price
    let long_result = this.state.long * this.props.trade.out.long_price
    let short_result = this.state.short * this.props.trade.out.short_price

    let long_yield = Number((((long_result - long_cost) / long_cost) * 100).toFixed(2))
    let short_yield = Number((((short_cost - short_result) / short_result) * 100).toFixed(2))

    let yieldPercent = (long_yield + short_yield) / 2


    if (_.isNaN(yieldPercent))
      return 0

    console.log(yieldPercent)

    return `${yieldPercent.toFixed(2)} %`
  }

  render() {
    return (
      <div className="row">
        <div className="col-lg-6">Expected Position Size</div>
        <div className="col-lg-6">
          <input type="text" defaultValue={this.state.size} onChange={this.setSize} />
        </div>
        <div className="col-lg-6">Position Size</div>
        <div className="col-lg-6">
          <span className="price_span">{this.calculatePositionSize()}</span>
        </div>
        {/* Shares */}
        <div className="col-lg-6">Nº Shares (Long / Short)</div>
        <div className="col-lg-6"> {this.state.long} / {this.state.short} </div>

        <div className="col-lg-6">P\L</div>
        <div className="col-lg-6"><span className="price_span">{this.calculatePandL()}</span></div>
        <div className="col-lg-6">Yield</div>
        <div className="col-lg-6"><span className="price_span">{this.calculateYield()}</span></div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  trade: state.trade
})

export default connect(mapStateToProps, {})(TradeDetail)