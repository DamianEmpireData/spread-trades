import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setChartClick } from '../actions/chartsActions'
import _ from 'lodash'
import moment from 'moment'

class TradeTable extends Component {
  constructor(props) {
    super(props)
    this.setEntry = this.setEntry.bind(this)
    this.setOut = this.setOut.bind(this)
  }

  setEntry() {
    this.props.setChartClick('entry')
  }

  setOut() {
    this.props.setChartClick('out')
  }

  showEntryBtn() {
    if (this.props.click === 'entry') {
      return (
        <span className="price_span">Active</span>
      )
    }

    return (
      <button className="btn btn-primary btn-sm" onClick={this.setEntry}>Set Price</button>
    )
  }

  calculateRatioChange() {

    let e_r = this.props.entry.ratio
    let o_r = this.props.out.ratio

    let p = Number((((o_r - e_r) / o_r) * 100).toFixed(2))

    if (_.isNaN(p)) {
      return 0
    }

    if (!_.isFinite(p)) {
      return -100
    }

    return p

  }

  calculateTimeFrame() {
    if (_.isEmpty(this.props.entry.date) || _.isEmpty(this.props.out.date)) {
      return '0 Days'
    }

    let e = moment(this.props.entry.date)
    let o = moment(this.props.out.date)

    return `${o.diff(e, 'days')} days`
  }

  showOutBtn() {
    if (this.props.click === 'out') {
      return (
        <span className="price_span">Active</span>
      )
    }

    return (
      <button className="btn btn-primary btn-sm" onClick={this.setOut}>Set Price</button>
    )
  }

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th></th>
            <th>Ratio</th>
            <th>Long</th>
            <th>Short</th>
            <th>Ratio %</th>
            <th>TimeFrame</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>Buy {this.showEntryBtn()}</th>
            <th>{this.props.entry.ratio}</th>
            <th>${this.props.entry.long_price}</th>
            <th>${this.props.entry.short_price}</th>
            <th></th>
            <th></th>
          </tr>
          <tr>
            <th> Sell {this.showOutBtn()}</th >
            <th>{this.props.out.ratio}</th>
            <th>${this.props.out.long_price}</th>
            <th>${this.props.out.short_price}</th>
            <th>{this.calculateRatioChange()} %</th>
            <th>{this.calculateTimeFrame()}</th>
          </tr>
        </tbody>
      </table>
    )
  }
}

const mapStateToProps = state => ({
  click: state.charts.click,
  entry: state.trade.entry,
  out: state.trade.out
})


export default connect(mapStateToProps, { setChartClick })(TradeTable)