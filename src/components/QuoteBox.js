import React, { Component } from 'react'

class QuoteBox extends Component {
  constructor(props) {
    super(props)

    this.isStock = this.isStock.bind(this)
  }

  isStock() {
    if(this.props.symbol)
    {
      return (
        <div className="row">
          <div className="col-lg-6">{this.props.type} - {this.props.symbol}</div>
          <div className="col-lg-6"><span className="price_span">${this.props.price}</span><br/></div>
        </div>
      )
    }

    return (
      <div className="row">
        <div className="col-lg-6">{this.props.type}</div>
        <div className="col-lg-6"><span className="price_span">{this.props.price}</span><br/></div>
      </div>
    )
  }

  

  render() {
    return (
      <div>
        {this.isStock()}
      </div>
    )
  }
}

export default QuoteBox;