import React, {
  Component
} from 'react';
import { connect } from 'react-redux'
import { setTickers } from '../actions/tickersActions'
import { resetTradeData } from '../actions/tradeActions'

class Tickers extends Component {
  constructor(props) {
    super(props)

    this.state = {
      long: 'lulu',
      short: 'nke'
    }

    this.setTickers = this.setTickers.bind(this)
    this.getData = this.getData.bind(this)
    this.swapTickers = this.swapTickers.bind(this)
  }

  componentDidMount() {
    this.getData()
  }

  setTickers(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  swapTickers () {
    this.setState({
      long: this.state.short,
      short: this.state.long
    })
    setTimeout(() => {
      this.getData()
    }, 100);
  }

  getData() {
    this.props.setTickers(this.state)
    this.props.resetTradeData()
  }

  render() {
    return (
     <div className="container-fluid" style={{padding: 10}}>
      <div className="row">
        <div className="col-lg-4">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">LONG</span>
            </div>
            <input type="text" value={this.state.long} name="long" className="form-control" placeholder="Ticker" onChange={this.setTickers} />
          </div>
        </div>
        <div className="col-lg-4">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">SHORT</span>
            </div>
            <input type="text" name="short" value={this.state.short} className="form-control" placeholder="Ticker" onChange={this.setTickers}/>
          </div>
        </div>
        <div className="col-lg-4">
          <button className="btn btn-default w-25" onClick={this.swapTickers}>O</button>
          <button className="btn btn-primary w-75" onClick={this.getData}>Get Data</button>
        </div>
      </div>
     </div>
    )
  } 
}

const mapStateToProps = state => ({
  tickers: state.tickers
})

export default connect(mapStateToProps, { setTickers, resetTradeData })(Tickers);