import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getQuotes } from '../actions/quotesActions'
import { setChartClick } from '../actions/chartsActions'
import QuoteBox from './QuoteBox'
import TradeTable from './TradeTable'
import TradeDetail from './TradeDetail'
import _ from 'lodash'

class Quotes extends Component {
  constructor(props) {
    super(props)

    this.getSpreadRatio = this.getSpreadRatio.bind(this)
  }

  componentWillReceiveProps(nextProps) {
      if(!_.isEqual(this.props.tickers, nextProps.tickers)) {
        this.props.getQuotes(nextProps.tickers)
      }

      return true
  }

  getSpreadRatio() {
    if(_.isEmpty(this.props.quotes.long)) {
      return ''
    }
    return (this.props.quotes.long.close / this.props.quotes.short.close).toFixed(2)
  }


  render() {
    return (
      <div className="container-fluid" style={{padding: 10, color: '#fff'}}>
      <div className="row">
        <div className="col-lg-4">
          <QuoteBox type="LONG" symbol={this.props.quotes.long.symbol} price={this.props.quotes.long.close} />
          <QuoteBox type="SHORT" symbol={this.props.quotes.short.symbol} price={this.props.quotes.short.close} />
          <QuoteBox type="Spread Ratio" price={this.getSpreadRatio()} />
        </div>
        <div className="col-lg-4">
            <TradeTable/>
        </div>
        <div className="col-lg-4">
          <TradeDetail/>
        </div>
      </div>
     </div>
    )
  }
}

const mapStateToProps = state => ({
  tickers: state.tickers,
  quotes: state.quotes,
  click: state.charts.click,
  trade: state.trade
})

export default connect(mapStateToProps, { getQuotes, setChartClick })(Quotes)