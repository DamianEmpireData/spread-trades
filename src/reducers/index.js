import { combineReducers } from 'redux'
import tickersReducers from './tickersReducer'
import quotesReducer from './quotesReducer'
import chartsReducers from './chartsReducers';
import tradeReducers from './tradeReducers'

export default combineReducers({
  tickers: tickersReducers,
  quotes: quotesReducer,
  charts: chartsReducers,
  trade: tradeReducers
})
