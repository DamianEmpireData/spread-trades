import { SET_ENTRY_DATA, SET_OUT_DATA, RESET_TRADE_DATA } from '../actions/types'

const initialState = {
  entry: {
    long_price: 0,
    short_price: 0,
    ratio: 0,
    date: ''
  },
  out: {
    long_price: 0,
    short_price: 0,
    ratio: 0,
    date: ''
  },
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_ENTRY_DATA:
      return {
        ...state, 
        entry: action.payload
      }
    case SET_OUT_DATA: {
      return {
        ...state, 
        out: action.payload
      }
    }
    case RESET_TRADE_DATA: {
      return {
        ...state,
        entry: initialState.entry,
        out: initialState.out
      }
    }
    default:
      return state
  }
}