import { SET_TICKERS } from '../actions/types'

const initialState = {
  long: '',
  short: ''
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_TICKERS:
      return {
        ...state,
        long: action.payload.long,
        short: action.payload.short
      }

    default:
      return state
  }
}