import { SET_CHARTS, SET_TIMEFRAME, SET_CHART_CLICK } from '../actions/types'

const initialState = {
  long: {},
  short: {},
  tf: 'ytd',
  click: 'entry'
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_CHARTS:
      return {
        ...state,
        long: action.payload.long,
        short: action.payload.short
      }
    case SET_TIMEFRAME: 
      return {
        ...state,
        tf: action.payload
      }
    case SET_CHART_CLICK: {
      return {
        ...state,
        click: action.payload
      }
    }
    default:
      return state
  }
}